/* **********
 * Models
 * ********** */

export interface Company {
  id: string;
  companyDomain: string;
  companyName: string;
  contactEmail: string;
  privacyPolicyUrl?: string;
}

export interface DataOffer {
  id: string;
  audience: string;
  dataType: string;
  companyId: string;
  purpose: string;
  termsAndConditions: string;
  offerBudget: number;
}

export interface User {
  id: string;
  credits: number;
}
