/* eslint-disable max-classes-per-file */
import level from 'level';
import type { LevelDB } from 'level';
import config from '../../config';
import logger from '../../utils/logger';
import lock from '../../utils/lock';

const exists = (value: any): boolean => typeof value !== 'undefined' && value !== null;

type Mutation<V> = (prevValue: V | undefined) => V;

/** A store that uses LevelDB */
export default class LevelStore<V> {
  private storeName: string;

  private db: LevelDB<string, string>;

  private errors: any;

  /** Creates a level store */
  constructor(storeName: string) {
    this.storeName = storeName;
    this.db = level<string, string>(`${config.levelDirectory}/${storeName}`, (err: any) => {
      this.errors = err;
    });
  }

  init() {
    if (this.errors) throw new Error(this.errors);
    else this.errors = undefined;
  }

  /** Retrieves the value parsed from the DB */
  async fetch(id: string): Promise<V> {
    try {
      const value = JSON.parse(await this.db.get(id));
      return !Array.isArray(value) && typeof value === 'object'
        ? { id, ...value } : value;
    } catch (e) {
      throw new Error(`${this.storeName} :: ${e.message}`);
    }
  }

  /** As fetch but retrives null if missing */
  async safeFetch(id: string, defaultValue?: V): Promise<V | undefined> {
    try {
      return await this.fetch(id);
    } catch {
      return defaultValue;
    }
  }

  /** Stores the value stringified in the DB */
  async store(id: string, obj: V): Promise<V> {
    try {
      const value = JSON.stringify(obj);
      await this.db.put(id, value);
      return obj;
    } catch (e) {
      logger.error(`${this.storeName} :: Can't store value ${JSON.stringify(obj)} for key ${id}`);
      throw e;
    }
  }

  /** Stores the value stringified on LevelDB only if the key didn't exist */
  async storeNonExistent(id: string, mutations: V): Promise<boolean> {
    return lock([[this.db, id]], async () => {
      if (await this.safeFetch(id)) return false;
      const newValue = typeof mutations === 'function' ? await mutations() : mutations;
      await this.store(id, newValue);
      return true;
    });
  }

  private async getUpdatedValue(
    id: string, mutations: Mutation<V> | V, defaultValue?: V,
  ): Promise<V> {
    const oldValue = await this.safeFetch(id, defaultValue);
    if (typeof mutations === 'function') {
      const mutationsFn = mutations as Mutation<V>;
      return mutationsFn(oldValue);
    }
    const oldValueExists = exists(oldValue);

    if ((!oldValueExists || Array.isArray(oldValue)) && Array.isArray(mutations)) {
      const expandableValue = oldValue as unknown as Array<any> | undefined;
      return [...(expandableValue || []), ...mutations] as unknown as V;
    }
    if ((!oldValueExists || typeof oldValue === 'object') && (!exists(mutations) || typeof mutations === 'object')) {
      return { ...oldValue, ...mutations };
    }
    return mutations;
  }

  /**
   * Updates the value stringified in the DB. If mutations is an array, it is appended.
   * If it is an object, it is merged. If it is a function, DB is overwritten with result. */
  async update(id: string, mutations: Mutation<V> | V, defaultValue?: V): Promise<V> {
    return lock([[this.db, id]], async () => {
      const updatedValue = await this.getUpdatedValue(id, mutations, defaultValue);
      return this.store(id, updatedValue);
    });
  }

  /** Stores a list of [key, value] */
  async storeList(list: Array<[string, V]>): Promise<{ [k: string]: V}> {
    await list.reduce(
      (batch, [k, v]) => batch.put(k, JSON.stringify(v)),
      this.db.batch(),
    ).write();
    return list.reduce((obj: { [k: string]: V }, [k, v]) => ({ ...obj, [k]: v }), {});
  }

  /** Deletes a list of keys */
  async deleteList(list: Array<string>): Promise<void> {
    await list.reduce((batch, k) => batch.del(k), this.db.batch()).write();
  }

  /** Updates a list of [key, value, defaultValue] */
  async updateList(
    list: Array<[string, Mutation<V> | V, V | undefined]>,
  ): Promise<{ [k: string]: V}> {
    return lock(list.map(([id]) => [this.db, id]), async () => {
      const updatesPromises = list.map(async ([id, mutations, defaultValue]) => {
        const updatedValue = await this.getUpdatedValue(id, mutations, defaultValue);
        return [id, updatedValue] as [string, V];
      });
      const updates = await Promise.all(updatesPromises);
      return this.storeList(updates);
    });
  }

  /** Removes data from the store. */
  async delete(id: string): Promise<void> {
    try {
      await this.db.del(id);
    } catch (e) {
      logger.error(`${this.storeName} :: Can't delete value for key ${id}`);
      throw e;
    }
  }

  private createListFunction<R>(mode: string):
  (query?: string | Function | Object, filter?: any) => Promise<Array<R>> {
    let format: Function;
    switch (mode) {
      case 'keys': format = (k: string) => k; break;
      case 'values': format = (v: string) => JSON.parse(v); break;
      default: format = (
        d: { key: string, value: string },
      ) => ({ id: d.key, ...JSON.parse(d.value) }); break;
    }
    const initialQuery = { keys: mode !== 'values', values: mode !== 'keys' };
    return (query?: string | Function | Object, filter?: any) => new Promise((resolve, reject) => {
      let finalQuery: any;
      switch (typeof query) {
        case 'string': finalQuery = { ...initialQuery, gte: query, lte: `${query}∞` }; break;
        case 'object': finalQuery = { ...initialQuery, ...query }; break;
        case 'function': finalQuery = { ...initialQuery, filter: query }; break;
        default: finalQuery = initialQuery; break;
      }
      let filterFunction = filter || finalQuery.filter;
      delete finalQuery.filter;
      if (typeof filterFunction === 'object') {
        const props = Object.entries(filterFunction);
        filterFunction = (d: any) => props.every(([k, v]) => d[k] === v);
      }
      const list: Array<R> = [];
      const selector = filterFunction
        ? async (data: any) => {
          const d = format(data);
          if (filterFunction(d)) {
            list.push(d);
          }
        } : (data: any) => list.push(format(data));
      this.db
        .createReadStream(finalQuery)
        .on('data', selector)
        .on('error', reject)
        .on('end', () => resolve(list));
    });
  }

  /** Lists all the objects stored in the DB */
  list = this.createListFunction<V & { id: string }>('all');

  /** Lists all the keys stored in the DB */
  listKeys = this.createListFunction<string>('keys');

  /** Lists all the values stored in the DB */
  listValues = this.createListFunction<V>('values');

  async asObject(): Promise<{ [id: string]: V & { id: string } }> {
    return (await this.list()).reduce((acc, item) => ({
      ...acc,
      [item.id]: item,
    }), {});
  }

  async count(query?: string | Function | Object, filter?: any): Promise<number> {
    return (await this.list(query, filter)).length;
  }

  filter(
    condition?: string | Function | Object,
  ): Promise<Array<{ id: string } & (V | { value: V })>> {
    if (!['function', 'object'].includes(typeof condition)) return this.list(condition);

    const formatItem = ({ key, value }: { key: string, value: string }) => {
      const val = JSON.parse(value);
      return {
        id: key,
        ...(typeof val === 'object' ? val : { value: val }),
      };
    };

    const includeItem = typeof condition === 'function'
      ? (data: any) => condition(data)
      : (data: any) => Object.entries(condition as Object).every(
        ([field, value]) => data[field] === value,
      );

    return new Promise((res, rej) => {
      const result: Array<{ id: string } & (V | { value: V })> = [];
      this.db
        .createReadStream({ keys: true, values: true })
        .on('data', (data) => {
          const item = formatItem(data);
          if (includeItem(item)) result.push(item);
        })
        .on('error', rej)
        .on('end', () => res(result));
    });
  }
}

export class NumericLevelStore extends LevelStore<number> {
  /** Stores the greatest value */
  async storeGreatest(id: string, newValue: number, defaultOldValue = 0) {
    return this.update(id, (oldValue) => Math.max(oldValue as number, newValue), defaultOldValue);
  }
}

export const enhance = <V>(
  store: LevelStore<V>,
  apply: (st: LevelStore<V>) => LevelStore<V>,
): LevelStore<V> => Object.assign(store, apply(store));
