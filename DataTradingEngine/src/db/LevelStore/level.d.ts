declare module 'level' {
  import type {
    AbstractLevelDOWN,
    AbstractIteratorOptions,
    AbstractIterator,
    AbstractGetOptions,
    ErrorCallback,
    ErrorValueCallback,
    AbstractChainedBatch,
    AbstractBatch,
    AbstractOptions,
  } from 'abstract-leveldown';

  import type { CodecOptions } from 'level-codec';

  import type { LevelUp } from 'levelup';

  namespace EncodingDown {
    interface GetOptions extends AbstractGetOptions, CodecOptions {}
    interface PutOptions extends AbstractOptions, CodecOptions {}
    interface DelOptions extends AbstractOptions, CodecOptions {}
    interface BatchOptions extends AbstractOptions, CodecOptions {}
    interface IteratorOptions extends AbstractIteratorOptions, CodecOptions {}
    interface ChainedBatch<K = any, V = any> extends AbstractChainedBatch<K, V> {
      write(cb: any): any;
      write(options: CodecOptions & AbstractOptions, cb: any): any;
    }
  }

  interface EncodingDown<K = any, V = any> extends AbstractLevelDOWN<K, V> {
    get(key: K, cb: ErrorValueCallback<V>): void;
    get(key: K, options: EncodingDown.GetOptions, cb: ErrorValueCallback<V>): void;

    put(key: K, value: V, cb: ErrorCallback): void;
    put(key: K, value: V, options: EncodingDown.PutOptions, cb: ErrorCallback): void;

    del(key: K, cb: ErrorCallback): void;
    del(key: K, options: EncodingDown.DelOptions, cb: ErrorCallback): void;

    batch(): EncodingDown.ChainedBatch;
    batch(array: AbstractBatch[], cb: ErrorCallback): EncodingDown.ChainedBatch;
    batch(
      array: AbstractBatch[], options: EncodingDown.BatchOptions, cb: ErrorCallback
    ): EncodingDown.ChainedBatch;

    iterator(options?: EncodingDown.IteratorOptions): AbstractIterator<any, any>;
  }

  export interface LevelDB<K = any, V = any> extends LevelUp<EncodingDown<K, V>> {
    errors: /* typeof levelerrors */ any; // ? level-errors is not in DT
  }

  function level<K = any, V = any>(
    location: string, options: any, callback?: ErrorCallback
  ): LevelDB<K, V>;

  function level<K = any, V = any>(location: string, options: any): LevelDB<K, V>;

  function level<K = any, V = any>(location: string): LevelDB<K, V>;

  export default level;
}
