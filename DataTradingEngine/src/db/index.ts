import { Company, DataOffer, User } from './models';
import LevelStore from './LevelStore';

export const users = new LevelStore<User>('users');
export const companies = new LevelStore<Company>('companies');
export const dataOffers = new LevelStore<DataOffer>('data-offers');

export const initStores = async () => {
  users.init();
  companies.init();
  dataOffers.init();
};
