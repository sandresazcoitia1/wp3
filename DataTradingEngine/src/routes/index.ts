import Router from 'express-promise-router';

import health from './health';
import accounts from './accounts';
import market from './market';

const router = Router();
router.use('/health', health);
router.use('/accounts', accounts);
router.use('/market', market);

export default router;
