import { Request } from 'express';
import Router from 'express-promise-router';
import { run } from '../middlewares/run';
import { NotImplementedError } from '../utils/error';

const router = Router();

router.get('/price', run(() => { throw new NotImplementedError('Not implemented!'); }));
router.get('/data-offers', run(() => { throw new NotImplementedError('Not implemented!'); }));

router.post('/data-offers', run(() => { throw new NotImplementedError('Not implemented!'); }, {
  params: (req: Request) => [req.body],
}));

export default router;
