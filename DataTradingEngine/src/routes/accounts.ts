import { Request } from 'express';
import Router from 'express-promise-router';
import { run } from '../middlewares/run';
import { NotImplementedError } from '../utils/error';

const router = Router();

router.get('/credits', run(() => { throw new NotImplementedError('Not implemented!'); }));

router.post('/credits', run(() => { throw new NotImplementedError('Not implemented!'); }, {
  params: (req: Request) => [req.body],
}));

export default router;
