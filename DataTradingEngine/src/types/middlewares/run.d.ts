import { ApiError } from '../../utils/error';

export interface Options {
  status?: number;
  emptyStatus?: number;
  ErrorType?: typeof ApiError;
  params?: Function;
  silent?: boolean;
}
