import { Request } from 'express';

export interface AuthUser {
}

export interface AuthRequest extends Request {
  user: AuthUser;
}
