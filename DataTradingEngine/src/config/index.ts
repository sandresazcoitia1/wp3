/* eslint-disable */
"use strict";

import dotenv from 'dotenv';

Object.defineProperty(exports, "__esModule", {
  value: true
});

dotenv.config({ path: process.env.ENV_PATH || '.env' })

const { env } = process;

const config = {
  app: {
    name: env.npm_package_name,
    version: env.npm_package_version
  },
  env: env.NODE_ENV,
  port: env.PORT,
  host: env.HOST,
  log: {
    error: env.ERROR_LOG,
    combined: env.COMBINED_LOG,
  },
  levelDirectory: env.LEVEL_DIRECTORY,
};

export default config;
