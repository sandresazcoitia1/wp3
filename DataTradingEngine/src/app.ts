import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import cors from 'cors';
import bodyParser from 'body-parser';
import boom from 'express-boom';
import swagger from 'swagger-tools';
import config from './config';
import logger from './utils/logger';
import errorHandler from './middlewares/errorHandler';
import routes from './routes';
import schema from './schema';

const app = express();

swagger.initializeMiddleware(schema, ({ swaggerMetadata, swaggerValidator, swaggerUi }) => {
  app.use(boom());
  app.use(swaggerMetadata());
  app.use(helmet());
  app.use(bodyParser.json());
  app.use(morgan('combined', {
    stream: logger.stream,
    skip: (req) => {
      if (req.originalUrl === '/health') return true;
      if (config.env === 'test') return true;
      return false;
    },
  }));
  app.use(cors());
  app.use(swaggerValidator());
  app.use(swaggerUi({ swaggerUi: '/api-docs', apiDocs: '/api-docs.json' }));
  app.use((error: Error, req: any, res: any, next: any) => { throw error; });
  app.use(routes);
  app.use(errorHandler); // This MUST always go after any other app.use(...)
});

export default app;
