/**
 * Stops execution of fn until resources are free
 * it will only lock the execution if there is a lock that shares some tag
 * or; the tag or the request locks the whole resource (no tags)
 * most common usage:
 *  lock([[resource]], async () =>{
 *    ...some async code with await...
 *  })
 * WARNING: a lock inside a lock can cause a deadlock
 *          if they both lock the same resource
 * @param {any[][]} requests A list of resource requests with tags
 * @param {(() => T) | any[][]} fn The code to lock until resources are free
 * @returns {Promise<T>} The result of fn
 * @template T
 */
export default function lock<T>(requests: any[][], fn: (() => T) | any[][]): Promise<T>;
