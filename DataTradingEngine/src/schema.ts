import swaggerJSDoc from 'swagger-jsdoc';
import glob from 'glob';

const schema = swaggerJSDoc({
  swaggerDefinition: {
    info: {
      title: 'PIMCity Data Trading Engine API',
      version: '0.1.0',
    },
  },
  apis: glob.sync(`${__dirname}/routes/**/*.js`),
});

export default schema;
