import { Request, Response, NextFunction } from 'express';
import logger from '../utils/logger';
import { ApiError } from '../utils/error';

// eslint-disable-next-line no-unused-vars
const errorHandler = async (err: Error, req: Request, res: Response, next: NextFunction) => {
  const { body } = req;
  if (err instanceof ApiError) {
    if (!err.silent) {
      logger.crit(`${err.message}\nCode: ${err.code}\n`, { body });
    }
    err.send(res);
  } else {
    logger.crit(err.stack || '', { body });
    res.boom.badImplementation();
  }
};

export default errorHandler;
