const express = require('express');
const estimationRoutController = require('../controllers/estimationRoutController');

const router = express.Router();

router.route('/').post(estimationRoutController.createAudience);

module.exports = router;
