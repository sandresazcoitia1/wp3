const mongoose = require('mongoose');
// const validator = require('validator');

const audienceSchema = new mongoose.Schema({
  ageMin: {
    type: Number,
    min: [13, 'the age should be between 13 and 65'],
    max: [65, 'the age should be between 13 and 65']
  },
  ageMax: {
    type: Number,
    min: [13, 'the age should be between 13 and 65'],
    max: [65, 'the age should be between 13 and 65']
  },
  interest: [
    {
      type: String
    }
  ],
  location: {
    type: String,
    required: [true, 'location is required (e.g. Spain)']
  },
  estimatedPrice: {
    type: Number,
    default: 5
  },
  queryDate: {
    type: Date,
    default: Date.now()
  }
});
const Audience = mongoose.model('Audience', audienceSchema);
module.exports = Audience;
