const request = require('request-promise');

class FBplatformHandler {
  constructor(
    acID,
    token,
    interest,
    country,
    gender,
    ageMin,
    ageMax,
    opetimizationGoal
  ) {
    this.acID = acID;
    this.token = token;
    this.interest = interest;
    this.country = country;
    this.gender = gender;
    this.ageMin = ageMin;
    this.ageMax = ageMax;
    this.opetimizationGoal = opetimizationGoal;
    this.point = 14;
  }

  async reqID() {
    const url = `https://graph.facebook.com/v3.2/search?access_token=${
      this.token
    }&locale=en_US&q=${this.interest}&type=adinterest&limit=100`;
    const results = await request.get(url);
    this.id = JSON.parse(results).data['0'].id;
    console.log(String(this.id));
    return String(this.id);
  }

  async genderprep() {
    if (this.gender === 'male') {
      this.gender = '1';
    } else if (this.gender === 'female') {
      this.gender = '2';
    } else {
      this.gender = '0';
    }
    return this.gender;
  }

  async priceEstimator() {
    const beginingPart = `https://graph.facebook.com/v7.0/act_`;
    const loggingPart = `${this.acID}/delivery_estimate?access_token=${
      this.token
    }`;
    const goalPart = `&optimization_goal=${this.opetimizationGoal}&`;
    let agemaxPart = `"age_max":${String(this.ageMax)},`;
    let ageminPart = `"age_min":${String(this.ageMin)},`;
    let genderPart = `"genders":[${this.gender}],`;
    const geolocationPart = `"geo_locations":{"countries":["${
      this.country
    }"],"location_types":["home"]}`;
    let interestPart = `,"interests":[{"id":'${this.interest}'}]`;
    if (this.gender === '0') {
      genderPart = '';
    }
    if (this.ageMax === 'MAX') {
      agemaxPart = '';
    }
    if (this.ageMin === 'MIN') {
      ageminPart = '';
    }
    if (this.interest === '0') {
      interestPart = '';
    }
    this.link = `${beginingPart}${loggingPart}${goalPart}targeting_spec={${ageminPart}${agemaxPart}${genderPart}${geolocationPart}${interestPart}}`;
    const results = await request.get(this.link);
    this.cost = JSON.parse(results).data[0].daily_outcomes_curve[
      this.point
    ].spend;
    this.impressions = JSON.parse(results).data[0].daily_outcomes_curve[
      this.point
    ].impressions;
    this.cpm = (this.cost * 1000) / this.impressions;
    console.log(this.cpm);
    return this.cpm;
  }
}

module.exports = FBplatformHandler;
