const dotenv = require('dotenv');
dotenv.config({ path: './config.env' });

const Audience = require('../models/audienceModel');
const FB = require('../platformHandler/FB');

exports.createAudience = async (req, res) => {
  try {
    const handler = new FB(
      process.env.FB_ACID,
      process.env.FB_TOKEN,
      req.body.interest,
      req.body.location,
      req.body.gender,
      req.body.ageMin,
      req.body.ageMax,
      'IMPRESSIONS'
    );
    handler.interest = await handler.reqID();
    handler.gender = await handler.genderprep();
    const estimateP = await handler.priceEstimator();
    req.body.estimatedPrice = estimateP;
    const newAudience = await Audience.create(req.body);
    newAudience.estimatedPrice = estimateP;
    res.status(201).json({
      status: 'success',
      data: {
        newAudience: newAudience
      }
    });
  } catch (err) {
    res.status(400).json({
      status: 'fail',
      message: err
    });
  }
};
