import logging from './lib/logging.js';
import puppeteer from "puppeteer";
const logger = logging.logger;

let useWpr = true;

let wprArgs = ['--ignore-certificate-errors-spki-list=PhrPvGIaAMmd29hj8BCZOq096yj7uMpRNHpn5PDxI6I=',
    '--host-resolver-rules="MAP *:80 127.0.0.1:8080,MAP *:443 127.0.0.1:8081,EXCLUDE localhost"'];

let puppeteerOptions = {
    headless: false,
    args: [
        '--window-size=1270x950',
        '--disable-crash-reporter'
    ],
    defaultViewport: null
};
let user = {
    username: 'USER@email.es',
    password: 'PASSWORD-GOES-HERE',
    userDataDir: 'chromium/devDataDir'
};

if (useWpr) {
    puppeteerOptions.args = puppeteerOptions.args.concat(wprArgs);
}
if(user.userDataDir) {
    puppeteerOptions.userDataDir = user.userDataDir;
}

(async () => {

    let browser, page;
    try {
        browser = await puppeteer.launch(puppeteerOptions);
        page = await browser.newPage();
        await page.goto('https://www.linkedin.com/campaignmanager/login').catch(function gotoError(error) { logger.warn('Campaign Manager Login window' + error) });
        const elementHandle = await page.$('.iframe--authentication');
        const authFrame = await elementHandle.contentFrame();

        await authFrame.$eval('input#username', element => element.value = '');
        await authFrame.type('input#username', user.username, {delay: 40});
        await authFrame.$eval('input#password', element => element.value = '');
        await authFrame.type('input#password', user.password, {delay: 70});
        await Promise.all([
            page.waitForNavigation({waitUntil: 'networkidle0'}),
            authFrame.click('.btn__primary--large', {delay: 2}),
        ]);
        await Promise.all([
            page.waitForNavigation({waitUntil: 'networkidle0'}),
            page.click('a[href*="/campaign-groups"]', {delay: 11}),
        ]);

        await Promise.all([
            page.waitForNavigation({waitUntil: 'networkidle0'}),
            page.click('div[data-control-name="create_campaign_header"] > button', {delay: 8}),
        ]);

    } catch (err) {
        let errMsg = `Error liUser.login - object for user ${user.username} not created`;
        logger.error(errMsg);
        logger.error(err.toString());
        if (browser) browser.close();
        throw new Error(errMsg);
    }

})();
