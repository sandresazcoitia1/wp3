import config from 'config';

import logging from '../lib/logging.js';
import LiUser from '../models/linkedin/liUser.js';
const logger = logging.logger;


export default class LiUsersService {
    constructor() {
        this.usersList = []
    }

    async populateFromConfig(){
        if (!config.has("linkedIn.users")) {
        }
        let configUsers = config.get("linkedIn.users");
        if (! Array.isArray(configUsers)) {
            logger.warn("linkedIn.users object in config is not of array type");
            return;
        }
        if (configUsers.length === 0) {
            logger.warn("linkedIn.users array in config is empty");
            return;
        }
        let addedUsers = 0;
        await Promise.all(configUsers.map(async (user) => {
            if (! this.usersList.includes(u => u.username === user.username)) {
                try {
                    let liUser = await LiUser.login(user);
                    this.addUser(liUser);
                    addedUsers++;
                } catch (err) {
                    logger.error("User %s could not login %s", user.username);
                }
            }
        }));
        if (addedUsers !== configUsers.length) logger.warn("No all users from config were added to the LiUsersService");
    }

    addUser(liUser) {
        this.usersList.push(liUser);
    }

    getUser() {
        let liUser = this.usersList.shift();
        this.usersList.push(liUser);
        return liUser;
    }
}
