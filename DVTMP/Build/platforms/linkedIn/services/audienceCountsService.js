import LiAudienceCountsDao from "../models/linkedin/liAudienceCountsDao.js";

export default class AudienceCountsService {
    constructor(container) {
        this.liAudienceCountsDao = container.get(LiAudienceCountsDao);
    }

    async getAudienceCounts(audienceData) {
        let result;
        //TODO: query first the local db to see if an entry matches the audienceData
        result = this.liAudienceCountsDao.getAudienceCounts(audienceData);
        //TODO: save the result to the local db for future use
        return result;
    }
}
