import LiBiddingLimitsDao from "../models/linkedin/liBiddingLimitsDao";

export default class BiddingLimitsService {
    constructor(container) {
        this.liBiddingLimitsDao = container.get(LiBiddingLimitsDao);
    }

    async getBiddingLimits(audienceData) {
        let result;
        //TODO: query first the local db to see if an entry matches the audienceData
        result = this.liBiddingLimitsDao.getBiddingLimits(audienceData);
        //TODO: save the result to the local db for future use
        return result;
    }
}
