import LiForecastingDao from "../models/linkedin/liForecastingDao";

export default class ForecastingService {
    constructor(container) {
        this.liForecastingDao = container.get(LiForecastingDao);
    }

    async getForecasting(audienceData) {
        let result;
        //TODO: query first the local db to see if an entry matches the audienceData
        result = this.liForecastingDao.getForecasting(audienceData);
        //TODO: save the result to the local db for future use
        return result;
    }
}
