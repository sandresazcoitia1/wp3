(include:
  (and:List(
    (or:List((facet:(urn:urn:li:adTargetingFacet:interfaceLocales,name:Interface Locales),segments:List((urn:urn:li:locale:en_US,name:English,facetUrn:urn:li:adTargetingFacet:interfaceLocales))))),
    (or:List(
      (facet:(urn:urn:li:adTargetingFacet:locations,name:Locations),
       segments:List(
         (urn:urn:li:geo:105646813,
          name:Spain,
          facetUrn:urn:li:adTargetingFacet:locations,
          ancestorUrns:List(urn:li:geo:100506914)
        )
       )
      )
    )),
    (or:List(
      (facet:(urn:urn:li:adTargetingFacet:genders,name:Member Gender),
       segments:List((urn:urn:li:gender:FEMALE,name:Female,facetUrn:urn:li:adTargetingFacet:genders),(urn:urn:li:gender:MALE,name:Male,facetUrn:urn:li:adTargetingFacet:genders))),
      (facet:(urn:urn:li:adTargetingFacet:ageRanges,name:Member Age),segments:List((urn:urn:li:ageRange:(18,24),name:18 to 24,facetUrn:urn:li:adTargetingFacet:ageRanges),(urn:urn:li:ageRange:(25,34),name:25 to 34,facetUrn:urn:li:adTargetingFacet:ageRanges)))
     ))
  )),
exclude:(or:List())
)



(include:
  (and:List(
    (or:List(
      (facet:(urn:urn:li:adTargetingFacet:interfaceLocales,
        name:Interface Locales),
        segments:List((urn:urn:li:locale:en_US,
                       name:English,
                       facetUrn:urn:li:adTargetingFacet:interfaceLocales))
        )
    )),
    (or:List(
      (facet:(urn:urn:li:adTargetingFacet:locations,name:Locations),
       segments:List((urn:urn:li:geo:105646813,name:Spain,facetUrn:urn:li:adTargetingFacet:locations,ancestorUrns:List(urn:li:geo:100506914)))
       )
    )),
    (or:List(
        (facet:(urn:urn:li:adTargetingFacet:genders,name:Member Gender),
            segments:List(
                (urn:urn:li:gender:FEMALE,name:Female,facetUrn:urn:li:adTargetingFacet:genders),
                (urn:urn:li:gender:MALE,name:Male,facetUrn:urn:li:adTargetingFacet:genders)
            )
        )
    )),
    (or:List(
        (facet:(urn:urn:li:adTargetingFacet:ageRanges,name:Member Age),
         segments:List(
            (urn:urn:li:ageRange:(18,24),name:18 to 24,facetUrn:urn:li:adTargetingFacet:ageRanges),
            (urn:urn:li:ageRange:(25,34),name:25 to 34,facetUrn:urn:li:adTargetingFacet:ageRanges)
        ))
    ))
  )),
exclude:(or:List())
)



(include:(and:List(
    (or:List((facet:(urn:urn:li:adTargetingFacet:interfaceLocales,name:Interface Locales),segments:List((urn:urn:li:locale:en_US,name:English,facetUrn:urn:li:adTargetingFacet:interfaceLocales))))),
    (or:List((facet:(urn:urn:li:adTargetingFacet:locations,name:Locations),
      segments:List(
        (urn:urn:li:geo:105646813,name:Spain,facetUrn:urn:li:adTargetingFacet:locations),
        (urn:urn:li:geo:105015875,name:France,facetUrn:urn:li:adTargetingFacet:locations,ancestorUrns:List(urn:li:geo:100506914))
      )
    )))
 )),
exclude:(or:List())
)



(include:(and:List((or:List((facet:(urn:urn:li:adTargetingFacet:interfaceLocales,name:Interface Locales),segments:List((urn:urn:li:locale:en_US,name:English,facetUrn:urn:li:adTargetingFacet:interfaceLocales))))),

or:List((facet:(urn:urn:li:adTargetingFacet:locations,name:Locations),
  segments:List(
    (urn:urn:li:geo:105646813,name:Spain,facetUrn:urn:li:adTargetingFacet:locations),
    (urn:urn:li:geo:105015875,name:France,facetUrn:urn:li:adTargetingFacet:locations,ancestorUrns:List(urn:li:geo:100506914)),
    (urn:urn:li:geo:91000000,name:European Union,facetUrn:urn:li:adTargetingFacet:locations,ancestorUrns:List()),
    (urn:urn:li:geo:100506914,name:Europe,facetUrn:urn:li:adTargetingFacet:locations,ancestorUrns:List()),
    (urn:urn:li:geo:91000002,name:European Economic Area,facetUrn:urn:li:adTargetingFacet:locations,ancestorUrns:List()),
    (urn:urn:li:geo:103335767,name:Community of Madrid, Spain,facetUrn:urn:li:adTargetingFacet:locations,ancestorUrns:List(urn:li:geo:105646813,urn:li:geo:100506914))
  )
))

))),exclude:(or:List()))
