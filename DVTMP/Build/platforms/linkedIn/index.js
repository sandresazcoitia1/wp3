// import config from 'config';
import typedi from "typedi";
let Container = typedi.Container;

import logging from './lib/logging.js';
import AudienceCountsService from "./services/audienceCountsService.js";
import LiUsersService from "./services/liUsersService.js";

const logger = logging.logger;

(async () => {
    let usersService = Container.get(LiUsersService);
    await usersService.populateFromConfig().then(() =>
        logger.info("UsersService populated from config")
    );
    let audienceCountsService = Container.get(AudienceCountsService);
})();
