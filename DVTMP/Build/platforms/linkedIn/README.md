# LinkedIn Data Valuation REST API

LinkedIn Data Valuation Tool for the backend using a REST API.

![block diagram](figures/block_diagram.png)

## Development

For development and testing the functionality against the LinkedIn Campaign Manager use [Catapult's Web Page Replay](https://chromium.googlesource.com/catapult/+/HEAD/web_page_replay_go/README.md).
WPR acts like a proxy that let you record and replay web sessions. Replays allow us to test part of the functionality faster and avoiding LinkedIn captcha's or other blockings.

#### Install go for Web Page Replay
Install go and configure GOPATH on your `.bashrc` or `.profile` file. Test that the GOPATH variable is loaded with `echo $GOPATH`.
I prefer to create a file in `/etc/profile.d/go.sh` to load a default GOPATH env variable if go is installed:

```bash
#!/usr/bin/env sh

if [ -f /usr/local/bin/go ] ; then
  export GOPATH=$(go env GOPATH)
fi
```


### Web Page Replay
#### Install

```bash
go get github.com/catapult-project/catapult/web_page_replay_go # Ignore the error that will print
go get -d github.com/urfave/cli
cd $GOPATH/src/github.com/urfave/cli
git checkout tags/v1.22.4
go install ./...
go get golang.org/x/net/http2
```

#### Record
Explorative sessions can be recorded running WPR in a bash console:
```bash
cd $HOME/go/src/github.com/catapult-project/catapult/web_page_replay_go
go run src/wpr.go record --http_port=8080 --https_port=8081 <path-to-linkedin-dvt-rest-api>/dev-audiences-session.wprgo
```

And the Puppeteer's Chromium from another console with the following options. Note that the `--user-data-dir` option has
 to match the one specified in the config file for that user (by default de development LinkedIn user in `config/dev-wpr.hjson`).

```bash
cd  <path-to-linkedin-dvt-rest-api>
node_modules/puppeteer/.local-chromium/linux-756035/chrome-linux/chrome --user-data-dir=chromium/devDataDir/ \
    --host-resolver-rules="MAP *:80 127.0.0.1:8080,MAP *:443 127.0.0.1:8081,EXCLUDE localhost" \
    --ignore-certificate-errors-spki-list=PhrPvGIaAMmd29hj8BCZOq096yj7uMpRNHpn5PDxI6I=
```
During the record session you have to log in on LinkedIn with a real user, but the proxy server will let you "authenticate"
in the replay sessions with dummy parameters for the username and password as long as they have a valid format.

#### Replay
The wpr command to run the proxy server to replay the sessions is 
```bash
cd $HOME/go/src/github.com/catapult-project/catapult/web_page_replay_go
go run src/wpr.go replay --http_port=8080 --https_port=8081 <path-to-linkedin-dvt-rest-api>/dev-audiences-session.wprgo
```

Once the wpr server is running, we can test our scripts against it using the proper Puppeteer configuration for wpr. 
For that, we set it on the file `config/dev-wpr.hsjon` and set the environment variable `NODE_CONFIG_ENV` to `dev-wpr`
```bash
export NODE_CONFIG_ENV="dev-wpr"
export NODE_OPTS="--experimental-json-modules"
node $NODE_OPTS /home/apastor/WebstormProjects/pimcity-linkedin/index.js
```
