import puppeteer from 'puppeteer';
import config from 'config';

import logging from '../../lib/logging.js';
const logger = logging.logger;

export default class LiUser {
    constructor(username, browser, liCampaignPage) {
        this.username = username;
        this.browser = browser;
        this.liCampaignPage = liCampaignPage;
    }
    static async login(user) {
        let browser, page;
        let puppeteerOptions = Object.assign({}, config.get('puppeteer'));
        if(user.userDataDir) {
            puppeteerOptions.userDataDir = user.userDataDir;
        }
        try {
            browser = await puppeteer.launch(puppeteerOptions);
            page = await browser.newPage();
            // await page.goto('https://www.google.es').catch(function gotoError(error) { logger.warn('Campaign Manager Login window' + error) });
            await page.goto('https://www.linkedin.com/campaignmanager/login').catch(function gotoError(error) { logger.warn('Campaign Manager Login window' + error) });
            const elementHandle = await page.$('.iframe--authentication');
            const authFrame = await elementHandle.contentFrame();

            await authFrame.$eval('input#username', element => element.value = '');
            await authFrame.type('input#username', user.username, {delay: 40});
            await authFrame.$eval('input#password', element => element.value = '');
            await authFrame.type('input#password', user.password, {delay: 70});
            await Promise.all([
                page.waitForNavigation({waitUntil: 'networkidle0'}),
                authFrame.click('.btn__primary--large', {delay: 2}),
            ]);
            await Promise.all([
                page.waitForNavigation({waitUntil: 'networkidle0'}),
                page.click('a[href*="/campaign-groups"]', {delay: 11}),
            ]);

            await Promise.all([
                page.waitForNavigation({waitUntil: 'networkidle0'}),
                page.click('div[data-control-name="create_campaign_header"] > button', {delay: 8}),
            ]);
        } catch (err) {
            let errMsg = `Error liUser.login - object for user ${user.username} not created`;
            logger.error(errMsg);
            logger.error(err.toString());
            if (browser) browser.close();
            throw new Error(errMsg);
        }
        return new LiUser(user.username, browser, page);
    }


    //TODO: Can LiUser.getAccountId() & LiUser.getCookies() be subtituted by getRequestParams()?
    getCookies() {
        //TODO: LiUser.getCookies
    }

    getAccountId() {
        //TODO: LiUser.getAccountId
    }

    async logout() {
        //TODO: LiUser.logout (priority -5)
    }
}
