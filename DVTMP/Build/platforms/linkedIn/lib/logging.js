import winston from 'winston';
import pjson from '../package.json';

export default {
    logger: winston.createLogger({
        level: 'info',
        format: winston.format.combine(
            winston.format.label({label: `[${pjson.name}]`}),
            winston.format.timestamp({
                format: 'YYYY-MM-DD HH:mm:ss'
            }),
            winston.format.json(),
            winston.format.splat(),
            winston.format.simple()
        ),
        //defaultMeta: { label: 'user-service' },
        transports: [
            new winston.transports.Console({
                format: winston.format.printf(info => {
                    let format = `${info.label}`;
                    format += ` ${info.timestamp} ${info.level.toUpperCase()}: ${info.message}`;
                    return format;
                })
            }),
            new winston.transports.File({filename: `../${pjson.name}.json.log`}),
            new winston.transports.File({
                format: winston.format.printf(info => {
                    let format = `${info.label}`;
                    format += ` ${info.timestamp} ${info.level.toUpperCase()}: ${info.message}`;
                    return format;
                }),
                filename: `../${pjson.name}.log`
            })
        ]
    })
};
